# Dockerを使ってみる

- Dockerをインストールしたあとで、 Dockerの動作を確認していくコマンド

```
$ docker -v
```

```
$ docker ps
```

```
$ docker run hello-world
```

```
$ docker run --name nginx -p 80:80 nginx:alpine
```

- ブラウザで `http://localhost` へアクセス
- アクセスできることを確認したら `Ctrl + C` で終了
- 再度、上のコマンドを実行しても、エラーになり起動できない
  - name: nginx のコンテナが既にあるため

```
$ docker run -d --rm --name nginx2 -p 81:80 nginx:alpine
```

- ブラウザで `http://localhost:81` へアクセス


# よく使うDockerのコマンド

## 基本コマンド

```
$ docker logs -f nginx2
```

```
$ docker images
```

```
$ docker ps
```

```
$ docker ps -a
```

```
$ docker start nginx
$ docker ps -a
```

```
$ docker stop nginx
$ docker stop nginx2
$ docker ps -a
```

## docker run のよく使うオプションを試してみる

```
$ docker run -d --rm --name nginx-volume -p 8080:80 -v `pwd`/www:/usr/share/nginx/html:ro nginx:alpine
```

- ブラウザで `http://localhost:8080` にアクセス
- wwwディレクトリの中の index.htmlが表示 (ボリュームマウントできている)


## コンテナ内に入る

```
$ docker exec -it nginx-volume sh
```

## コンテナ・イメージを削除

```
$ docker ps -a                # コンテナ一覧を確認
$ docker rm nginx             # nginxコンテナを削除
$ docker rm d0f28a9264c6      # hello-world のDockerイメージを起動したコンテナをIDで削除(IDが変更しているはずなので `docker ps` で確認したIDをセットすること)
$ docker stop nginx-volume    # nginx-volumeを停止（ --rm オプションがあるので自動でコンテナを削除）
$ docker rm nginx-volume      # 自動で削除されているのでエラー（そんなコンテナは無い）
$ docker ps -a                # 全てのコンテナが削除されている
```

- nginxコンテナを削除したので `docker run --name nginx -p 80:80 nginx` が実行できるようになっている

```
$ docker images                   # Dockerイメージの確認
$ docker rmi hello-world:latest   # hello-worldイメージを削除（latestはタグ）
$ docker rmi 719cd2e3ed04         # nginxイメージをIDで削除
$ docker images
```

