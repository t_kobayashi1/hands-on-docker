# Dockerfileを書いてみる

- `dockerfile-test` ディレクトリに移動して、dockerfile を書きます
  - www ディレクトリをマウントした Nginx のイメージを作ってください
  - ブラウザでアクセスできることを確認してください


--------

```
$ docker build -t sample-web .
$ docker run -d --rm --name sample-web -p 80:80 sample-web
```
```
$ docker stop sample-web
$ docker rmi sample-web
```


--------

# Docker Registry にイメージをプッシュ


```
$ docker login registry.gitlab.com
$ docker build -t registry.gitlab.com/koda3t/hands-on-docker .
$ docker push registry.gitlab.com/koda3t/hands-on-docker
```

- プルしてみる

```
$ docker images
$ docker rmi registry.gitlab.com/koda3t/hands-on-docker
$ docker images
$ docker run -d --rm --name sample-web -p 80:80 registry.gitlab.com/koda3t/hands-on-docker # ローカルに無いのでPullする
```

```
$ docker stop sample-web
```