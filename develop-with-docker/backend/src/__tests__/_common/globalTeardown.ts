import { close } from '../../db/connection'

module.exports = async function() {
  // console.log('disconnect from database')
  await close()
}
