# Docker, Docker-Composeを使った開発

## Backend
- Setup
```
$ docker-compose run --rm backend yarn install --no-optional
```

- lint
```
$ docker-compose run --rm backend yarn lint
$ docker-compose run --rm backend yarn lint:fix
```

- migration database
```
$ docker-compose run --rm backend yarn run typeorm migration:run
```

- test
```
$ docker-compose down -v && docker-compose run --rm backend yarn run typeorm schema:sync && docker-compose run --rm backend yarn test:coverage
```


--------

## Frontend
- Setup
```
$ docker-compose run --rm frontend yarn install --no-optional
```

- lint
```
$ docker-compose run --rm frontend yarn lint
```

--------


## Run service
- run
```
$ docker-compose up -d
```

- stop
```
$ docker-compose down
```

- if you want reset database, you can use `-v` option
```
$ docker-compose down -v
```

